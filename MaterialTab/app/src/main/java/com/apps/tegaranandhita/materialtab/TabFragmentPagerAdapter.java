package com.apps.tegaranandhita.materialtab;

/**
 * Created by Tegar Anandhita on 09/03/2017.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.apps.tegaranandhita.materialtab.fragment.Tab1Fragment;
import com.apps.tegaranandhita.materialtab.fragment.Tab2Fragment;
import com.apps.tegaranandhita.materialtab.fragment.Tab3Fragment;

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {
    //nama tab nya
    String[] title = new String[]{
            "Modul 1", "Modul 2","Modul 3"
    };

    public TabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    //method ini yang akan memanipulasi penampilan Fragment dilayar
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new Tab1Fragment();
                break;
            case 1:
                fragment = new Tab2Fragment();
                break;
            case 2:
                fragment = new Tab3Fragment();
                break;
            default:
                fragment = null;
                break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }
}